package ir.malv.testpushpole1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pushpole.sdk.PushPole

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        PushPole.initialize(this, true)
    }
}
