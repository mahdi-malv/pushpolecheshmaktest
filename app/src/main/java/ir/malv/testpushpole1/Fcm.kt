package ir.malv.testpushpole1

import com.google.firebase.messaging.RemoteMessage
import com.pushpole.sdk.PushPole

import me.cheshmak.android.sdk.core.push.CheshmakFirebaseMessagingService
import java.lang.Exception


class Fcm : CheshmakFirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        println("Message: ${remoteMessage.data}")
        if (isCheshmakMessage(remoteMessage)) {
            super.onMessageReceived(remoteMessage)
        } else {
            if (PushPole.getFcmHandler(this).onMessageReceived(remoteMessage)) {
                // Message if for PushPole
                return
            } else {
                // Neither PushPole or Cheshmak
            }
        }
    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        PushPole.getFcmHandler(this).onNewToken(p0)
    }

    override fun onMessageSent(p0: String?) {
        super.onMessageSent(p0)
        PushPole.getFcmHandler(this).onMessageSent(p0)
    }

    override fun onSendError(p0: String?, p1: Exception?) {
        super.onSendError(p0, p1)
        PushPole.getFcmHandler(this).onSendError(p0, p1)
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
        PushPole.getFcmHandler(this).onDeletedMessages()
    }
}