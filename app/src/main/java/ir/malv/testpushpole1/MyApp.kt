package ir.malv.testpushpole1

import android.app.Application
import me.cheshmak.android.sdk.core.Cheshmak

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Cheshmak.with(this);
        Cheshmak.initTracker("D7f1GRjGlK5SHuokEhjsgQ==");
    }
}